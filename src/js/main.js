const inputFile = document.getElementById("file-input");
const iconsContent = document.getElementById("icons");
const fileContent = document.getElementById("file");
const buttonClose = document.getElementById("close");
const sizes = [
    { size: 48, name: 'mdpi' },
    { size: 72, name: 'hdpi' },
    { size: 96, name: 'xhdpi' },
    { size: 144, name: 'xxhdpi' },
    { size: 192, name: 'xxxhdpi' }
];

var nameImage = "ic_";
var imageFile;
function ProcessImage(file) {
    const reader = new FileReader();
    nameImage += file.name;

    reader.addEventListener('load', e => {
        imageFile = new Image();
        imageFile.src = e.target.result;

        imageFile.onload = onloadImage;
    });

    reader.readAsDataURL(file);
}

function onloadImage() {
    var w = this.width;
    var h = this.height;

    for (var i = sizes.length - 1; i >= 0; i--) {
        var s = sizes[i].size;
        var height = ((s * h) / w);
        InjectImage(sizes[i].name, ((s - height) / 2), (s - 1), height);
    }

    iconsContent.style.display = 'flex';
    fileContent.style.display = 'none';
}

function InjectImage(name, y, width, height) {
    var canvas = document.getElementById(name);
    canvas.getContext('2d').drawImage(imageFile, 1, y, width, height);

    canvas.toBlob(blob => {
        var a = document.getElementById(`${name}-link`);
        a.href = URL.createObjectURL(blob);
        a.download = nameImage;
    });
}

buttonClose.addEventListener("click", e => {
    for (var i = sizes.length - 1; i >= 0; i--) {
        var name = sizes[i].name;
        var size = sizes[i].size;
        var canvas = document.getElementById(name);
        var a = document.getElementById(`${name}-link`);
        canvas.getContext('2d').clearRect(0, 0, size, size);
        a.removeAttribute('href');
        a.removeAttribute('download');
    }

    nameImage = "ic_";
    inputFile.value = null;
    iconsContent.style.display = 'none';
    fileContent.style.display = 'block';
});

inputFile.addEventListener('change', e => {
    ProcessImage(e.target.files[0]);
});

