fileContent.addEventListener("dragover", e => {
    e.preventDefault();
});

fileContent.addEventListener("drop", e => {
    e.preventDefault();
    ProcessImage(e.dataTransfer.items[0].getAsFile());
});

fileContent.addEventListener("click", e => {
    inputFile.click();
});